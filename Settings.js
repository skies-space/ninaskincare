let pathPrefix = {
    bundle:"dist/bundle",
    html:"dist/",
    src:"src"
};

module.exports = {
    PathConfig : {
        src:{
            html    :{
                watch:`${pathPrefix.src}/templates/**/*.pug`,
                build:[
                    `!${pathPrefix.src}/templates/includes/**/*`,
                    `!${pathPrefix.src}/templates/pages/**/*`,
                    `${pathPrefix.src}/templates/*.pug`
                ]
            },
            js      :{
                sources:`${pathPrefix.src}/javascript/**/*.js`,
                lib:`${pathPrefix.src}/javascriptLibs/**/*`
            },
            css     :`${pathPrefix.src}/sass/**/*.scss`,
            static  :{
                fonts   : `${pathPrefix.src}/fonts/**/*`,
                images  : `${pathPrefix.src}/images/**/*`,
            }
        },
        dest:{
            html    :pathPrefix.html,
            js      :{
                sources:`${pathPrefix.bundle}/js/`,
                lib:`${pathPrefix.bundle}/jsLibs/`
            },
            css     :`${pathPrefix.bundle}/css`,
            static  :{
                fonts   : `${pathPrefix.bundle}/static/fonts`,
                images  : `${pathPrefix.bundle}/static/images`,
            }
        }
    },
    jsLibs : {
        src:{
            jquery:"node_modules/jquery/dist/**/*",
            hamburgers:"node_modules/hamburgers/_sass/hamburgers/**/*",
            tiny_slider:"node_modules/tiny-slider/dist/**/*"
        },
        dest:{
            jquery:`${pathPrefix.src}/javascriptLibs/jquery/`,
            hamburgers:`${pathPrefix.src}/sass/hamburgers/`,
            tiny_slider:`${pathPrefix.src}/javascriptLibs/tinyslider/`
        }
    }
};