####Sources description:

* "/src/fonts/"      - all project fonts, builds in "bundle/static/fonts"
* "/src/images/'     - all project images, builds in "bundle/static/images"
* "/src/javascript/" - js sources, builds in "bundle/js"
* "/src/javascriptLibs/" - js libs, builds in "bundle/static/jsLibs" 
* "/src/sass/" - css sources(prod. scss, for build tricky shapes) "bundle/static/jsLibs"
* "/src/templates/" - html templates(prod. by pug, just for fun), "../"
* "./Settings.js" - project settings what requires in gulp(mainly for paths config)


####Some other description:

Building project implements by "gulp". All gulp-tasks are named for their purpose.
* For install project files from git use `yarn` command in proj. root and after that `yarn install`
* For watch use `yarn watch`
