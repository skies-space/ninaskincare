let PageItems = {
    slider:"#vignette-slider",
    sliderCounter:{
        current:".slides-counter .current-slide",
        allitems:".slides-counter .slides-count"
    },
    sliderControl:{
        next:".arrow-wrapper#next",
        prev:".arrow-wrapper#prev"
    }
};
$(document).ready(function () {
    let slider = tns({
        container: PageItems.slider,
        items: 1,
        slideBy: 1,
        autoplay: true,
        controls:false,
        nav:false,
        autoplayButtonOutput:false,
    });
    slider.goTo(1);
    $(PageItems.sliderCounter.allitems).text(`/ ${slider.getInfo().slideCount}`);
    $(PageItems.sliderCounter.current).text(1);
    slider.events.on('transitionEnd',function () {
        console.log(slider.getInfo());
        $(PageItems.sliderCounter.current).text(slider.getInfo().displayIndex);
    });
    $(PageItems.sliderControl.next).on("click", function () {
        slider.goTo("next");
    });
    $(PageItems.sliderControl.prev).on("click", function () {
        slider.goTo("prev");
    });
});