const projectSettings   = require("./Settings");
const gulp              = require("gulp");
const browserSync       = require("browser-sync").create();
const pug               = require("gulp-pug");//html
const sass              = require("gulp-sass");//css
const autoprefixer      = require("gulp-autoprefixer");
const babel             = require("gulp-babel");//javascript

gulp.task("html-install", function(){
    return gulp.src(projectSettings.PathConfig.src.html.build)
        .pipe(pug({
            pretty: '\t'
        }))
        .pipe(gulp.dest(projectSettings.PathConfig.dest.html))
        .pipe(browserSync.stream());
});
gulp.task("watch-html", function(){
    return gulp.watch(projectSettings.PathConfig.src.html.watch, gulp.series("html-install"));
});

gulp.task("css-install", function(){
    return gulp.src(projectSettings.PathConfig.src.css)
        .pipe(sass())
        .pipe(autoprefixer({cascade: false}))
        .pipe(gulp.dest(projectSettings.PathConfig.dest.css))
        .pipe(browserSync.stream());
});
gulp.task("watch-css", function(){
    return gulp.watch(projectSettings.PathConfig.src.css, gulp.series("css-install"));
});

gulp.task("add.lib:TinySlider", function () {
    return gulp.src(projectSettings.jsLibs.src.tiny_slider)
        .pipe(gulp.dest(projectSettings.jsLibs.dest.tiny_slider));
});
gulp.task("add.lib:hamburgers", function () {
    return gulp.src(projectSettings.jsLibs.src.hamburgers)
        .pipe(gulp.dest(projectSettings.jsLibs.dest.hamburgers));
});
gulp.task("add.lib:jquery", function () {
    return gulp.src(projectSettings.jsLibs.src.jquery)
        .pipe(gulp.dest(projectSettings.jsLibs.dest.jquery));
});
gulp.task("jsLibs-add", gulp.series(
    gulp.parallel([
        "add.lib:hamburgers",
        "add.lib:jquery",
        "add.lib:TinySlider"
    ])
));
gulp.task("jsLibs-install", function(){
    return gulp.src(projectSettings.PathConfig.src.js.lib)
        .pipe(gulp.dest(projectSettings.PathConfig.dest.js.lib));
});
gulp.task("watch-jsLibs", function(){
    return gulp.watch(projectSettings.PathConfig.src.js.lib, gulp.series("jsLibs-install"));
});

gulp.task("js-install", function(){
    return gulp.src(projectSettings.PathConfig.src.js.sources)
        .pipe(babel({
            presets: ["@babel/preset-env"]
        }))
        .pipe(gulp.dest(projectSettings.PathConfig.dest.js.sources))
        .pipe(browserSync.stream());
});
gulp.task("watch-js", function(){
    return gulp.watch(projectSettings.PathConfig.src.js.sources, gulp.series("js-install"));
});


gulp.task("fonts-install", function () {
    return gulp.src(projectSettings.PathConfig.src.static.fonts).
        pipe(gulp.dest(projectSettings.PathConfig.dest.static.fonts));
});
gulp.task("watch-fonts", function(){
    return gulp.watch(projectSettings.PathConfig.src.static.fonts, gulp.series("fonts-install"));
});

gulp.task("images-install", function () {
    return gulp.src(projectSettings.PathConfig.src.static.images).
    pipe(gulp.dest(projectSettings.PathConfig.dest.static.images));
});
gulp.task("watch-images", function(){
    return gulp.watch(projectSettings.PathConfig.src.static.images, gulp.series("images-install"));
});

gulp.task("StaticContent-install", gulp.series(
    gulp.parallel([
        "fonts-install",
        "images-install"
    ])
));
gulp.task("watch-StaticContent", function () {
    return gulp.watch(projectSettings.PathConfig.src.static.images, gulp.series(
        gulp.parallel([
            "watch-fonts",
            "watch-images"
        ])
    ));
});

gulp.task("browserSync-start",  function(){
    browserSync.init({
        server: {
          baseDir:projectSettings.PathConfig.dest.html
        },
        port: 8080
      });
});

gulp.task("install", gulp.series(
    gulp.parallel([
        'StaticContent-install',
        'html-install',
        'css-install',
        gulp.series(['jsLibs-add', 'jsLibs-install']),
        'js-install',
    ])
));
gulp.task("watch", gulp.series(
    gulp.parallel([
        'watch-StaticContent',
        'watch-jsLibs',
        'watch-html',
        'watch-css',
        'watch-js',
        "browserSync-start"
    ])
));